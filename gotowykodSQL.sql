/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2012                    */
/* Created on:     02.12.2020 17:59:34                          */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('GRUPA') and o.name = 'FK_GRUPA_SEMESTR -_SEMESTR')
alter table GRUPA
   drop constraint "FK_GRUPA_SEMESTR -_SEMESTR"
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('"Grupa - Przedmiot"') and o.name = 'FK_GRUPA - _GRUPA - P_GRUPA')
alter table "Grupa - Przedmiot"
   drop constraint "FK_GRUPA - _GRUPA - P_GRUPA"
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('"Grupa - Przedmiot"') and o.name = 'FK_GRUPA - _GRUPA - P_PRZEDMIO')
alter table "Grupa - Przedmiot"
   drop constraint "FK_GRUPA - _GRUPA - P_PRZEDMIO"
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('OCENA') and o.name = 'FK_OCENA_OCENA - P_PRZEDMIO')
alter table OCENA
   drop constraint "FK_OCENA_OCENA - P_PRZEDMIO"
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('OCENA') and o.name = 'FK_OCENA_OCENA - S_STUDENT')
alter table OCENA
   drop constraint "FK_OCENA_OCENA - S_STUDENT"
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PROWADZACY') and o.name = 'FK_PROWADZA_PENSJA - _PENSJA')
alter table PROWADZACY
   drop constraint "FK_PROWADZA_PENSJA - _PENSJA"
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRZEDMIOT') and o.name = 'FK_PRZEDMIO_PRZEDMIOT_PROWADZA')
alter table PRZEDMIOT
   drop constraint FK_PRZEDMIO_PRZEDMIOT_PROWADZA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRZEDMIOT') and o.name = 'FK_PRZEDMIO_PRZEDMIOT_SEMESTR')
alter table PRZEDMIOT
   drop constraint FK_PRZEDMIO_PRZEDMIOT_SEMESTR
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('"Prowadzacy - Student"') and o.name = 'FK_PROWADZA_PROWADZAC_PROWADZA')
alter table "Prowadzacy - Student"
   drop constraint FK_PROWADZA_PROWADZAC_PROWADZA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('"Prowadzacy - Student"') and o.name = 'FK_PROWADZA_PROWADZAC_STUDENT')
alter table "Prowadzacy - Student"
   drop constraint FK_PROWADZA_PROWADZAC_STUDENT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('STUDENT') and o.name = 'FK_STUDENT_GRUPA - S_GRUPA')
alter table STUDENT
   drop constraint "FK_STUDENT_GRUPA - S_GRUPA"
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('STUDENT') and o.name = 'FK_STUDENT_STUDENT -_TRYB_NAU')
alter table STUDENT
   drop constraint "FK_STUDENT_STUDENT -_TRYB_NAU"
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('GRUPA')
            and   name  = 'Semestr - Grupa_FK'
            and   indid > 0
            and   indid < 255)
   drop index GRUPA."Semestr - Grupa_FK"
go

if exists (select 1
            from  sysobjects
           where  id = object_id('GRUPA')
            and   type = 'U')
   drop table GRUPA
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('"Grupa - Przedmiot"')
            and   name  = 'Grupa - Przedmiot2_FK'
            and   indid > 0
            and   indid < 255)
   drop index "Grupa - Przedmiot"."Grupa - Przedmiot2_FK"
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('"Grupa - Przedmiot"')
            and   name  = 'Grupa - Przedmiot_FK'
            and   indid > 0
            and   indid < 255)
   drop index "Grupa - Przedmiot"."Grupa - Przedmiot_FK"
go

if exists (select 1
            from  sysobjects
           where  id = object_id('"Grupa - Przedmiot"')
            and   type = 'U')
   drop table "Grupa - Przedmiot"
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('OCENA')
            and   name  = 'Ocena - Przedmiot_FK'
            and   indid > 0
            and   indid < 255)
   drop index OCENA."Ocena - Przedmiot_FK"
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('OCENA')
            and   name  = 'Ocena - Student_FK'
            and   indid > 0
            and   indid < 255)
   drop index OCENA."Ocena - Student_FK"
go

if exists (select 1
            from  sysobjects
           where  id = object_id('OCENA')
            and   type = 'U')
   drop table OCENA
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PENSJA')
            and   type = 'U')
   drop table PENSJA
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('PROWADZACY')
            and   name  = 'Pensja - Prowadzacy_FK'
            and   indid > 0
            and   indid < 255)
   drop index PROWADZACY."Pensja - Prowadzacy_FK"
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PROWADZACY')
            and   type = 'U')
   drop table PROWADZACY
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('PRZEDMIOT')
            and   name  = 'Przedmiot - Semestr_FK'
            and   indid > 0
            and   indid < 255)
   drop index PRZEDMIOT."Przedmiot - Semestr_FK"
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('PRZEDMIOT')
            and   name  = 'Przedmiot  - Prowadzacy_FK'
            and   indid > 0
            and   indid < 255)
   drop index PRZEDMIOT."Przedmiot  - Prowadzacy_FK"
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PRZEDMIOT')
            and   type = 'U')
   drop table PRZEDMIOT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('"Prowadzacy - Student"')
            and   name  = 'Prowadzacy - Student2_FK'
            and   indid > 0
            and   indid < 255)
   drop index "Prowadzacy - Student"."Prowadzacy - Student2_FK"
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('"Prowadzacy - Student"')
            and   name  = 'Prowadzacy - Student_FK'
            and   indid > 0
            and   indid < 255)
   drop index "Prowadzacy - Student"."Prowadzacy - Student_FK"
go

if exists (select 1
            from  sysobjects
           where  id = object_id('"Prowadzacy - Student"')
            and   type = 'U')
   drop table "Prowadzacy - Student"
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SEMESTR')
            and   type = 'U')
   drop table SEMESTR
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('STUDENT')
            and   name  = 'Grupa - Student_FK'
            and   indid > 0
            and   indid < 255)
   drop index STUDENT."Grupa - Student_FK"
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('STUDENT')
            and   name  = 'Student - Tryb_nauki_FK'
            and   indid > 0
            and   indid < 255)
   drop index STUDENT."Student - Tryb_nauki_FK"
go

if exists (select 1
            from  sysobjects
           where  id = object_id('STUDENT')
            and   type = 'U')
   drop table STUDENT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TRYB_NAUKI')
            and   type = 'U')
   drop table TRYB_NAUKI
go

/*==============================================================*/
/* Table: GRUPA                                                 */
/*==============================================================*/
create table GRUPA (
   ID_grupa             numeric(10)          identity,
   ID_semestr           numeric(10)          null,
   Nazwa_grupa          varchar(20)          null,
   constraint PK_GRUPA primary key nonclustered (ID_grupa)
)
go

/*==============================================================*/
/* Index: "Semestr - Grupa_FK"                                  */
/*==============================================================*/
create index "Semestr - Grupa_FK" on GRUPA (
ID_semestr ASC
)
go

/*==============================================================*/
/* Table: "Grupa - Przedmiot"                                   */
/*==============================================================*/
create table "Grupa - Przedmiot" (
   ID_grupa             numeric(10)          not null,
   ID_przedmiot         numeric(10)          not null,
   constraint "PK_GRUPA - PRZEDMIOT" primary key (ID_grupa, ID_przedmiot)
)
go

/*==============================================================*/
/* Index: "Grupa - Przedmiot_FK"                                */
/*==============================================================*/
create index "Grupa - Przedmiot_FK" on "Grupa - Przedmiot" (
ID_grupa ASC
)
go

/*==============================================================*/
/* Index: "Grupa - Przedmiot2_FK"                               */
/*==============================================================*/
create index "Grupa - Przedmiot2_FK" on "Grupa - Przedmiot" (
ID_przedmiot ASC
)
go

/*==============================================================*/
/* Table: OCENA                                                 */
/*==============================================================*/
create table OCENA (
   ID_ocena             numeric(10)          identity,
   ID_przedmiot         numeric(10)          null,
   ID_student           numeric(15)          null,
   Ocena                numeric(2,2)         null,
   Data_oceny           datetime             null,
   Rodzaj_oceny         varchar(10)          null,
   constraint PK_OCENA primary key nonclustered (ID_ocena)
)
go

/*==============================================================*/
/* Index: "Ocena - Student_FK"                                  */
/*==============================================================*/
create index "Ocena - Student_FK" on OCENA (
ID_student ASC
)
go

/*==============================================================*/
/* Index: "Ocena - Przedmiot_FK"                                */
/*==============================================================*/
create index "Ocena - Przedmiot_FK" on OCENA (
ID_przedmiot ASC
)
go

/*==============================================================*/
/* Table: PENSJA                                                */
/*==============================================================*/
create table PENSJA (
   ID_pensja            numeric              identity,
   Kwota_pensji         decimal(7,2)         null,
   constraint PK_PENSJA primary key nonclustered (ID_pensja)
)
go

/*==============================================================*/
/* Table: PROWADZACY                                            */
/*==============================================================*/
create table PROWADZACY (
   ID_prowadzacy        numeric              identity,
   ID_pensja            numeric              null,
   Data_urodzenia       datetime             null,
   Tytul_naukowy        varchar(20)          null,
   Imie_prowadzacy      varchar(20)          null,
   Nazwisko_prowadzacy  varchar(15)          null,
   constraint PK_PROWADZACY primary key nonclustered (ID_prowadzacy)
)
go

/*==============================================================*/
/* Index: "Pensja - Prowadzacy_FK"                              */
/*==============================================================*/
create index "Pensja - Prowadzacy_FK" on PROWADZACY (
ID_pensja ASC
)
go

/*==============================================================*/
/* Table: PRZEDMIOT                                             */
/*==============================================================*/
create table PRZEDMIOT (
   ID_przedmiot         numeric(10)          identity,
   ID_semestr           numeric(10)          null,
   ID_prowadzacy        numeric              null,
   Nazwa_przedmiot      varchar(20)          null,
   constraint PK_PRZEDMIOT primary key nonclustered (ID_przedmiot)
)
go

/*==============================================================*/
/* Index: "Przedmiot  - Prowadzacy_FK"                          */
/*==============================================================*/
create index "Przedmiot  - Prowadzacy_FK" on PRZEDMIOT (
ID_prowadzacy ASC
)
go

/*==============================================================*/
/* Index: "Przedmiot - Semestr_FK"                              */
/*==============================================================*/
create index "Przedmiot - Semestr_FK" on PRZEDMIOT (
ID_semestr ASC
)
go

/*==============================================================*/
/* Table: "Prowadzacy - Student"                                */
/*==============================================================*/
create table "Prowadzacy - Student" (
   ID_prowadzacy        numeric              not null,
   ID_student           numeric(15)          not null,
   constraint "PK_PROWADZACY - STUDENT" primary key (ID_prowadzacy, ID_student)
)
go

/*==============================================================*/
/* Index: "Prowadzacy - Student_FK"                             */
/*==============================================================*/
create index "Prowadzacy - Student_FK" on "Prowadzacy - Student" (
ID_prowadzacy ASC
)
go

/*==============================================================*/
/* Index: "Prowadzacy - Student2_FK"                            */
/*==============================================================*/
create index "Prowadzacy - Student2_FK" on "Prowadzacy - Student" (
ID_student ASC
)
go

/*==============================================================*/
/* Table: SEMESTR                                               */
/*==============================================================*/
create table SEMESTR (
   ID_semestr           numeric(10)          identity,
   Nr_semestr           int                  null,
   Data_rozpoczecia     datetime             null,
   Data_zakonczenia     datetime             null,
   constraint PK_SEMESTR primary key nonclustered (ID_semestr)
)
go

/*==============================================================*/
/* Table: STUDENT                                               */
/*==============================================================*/
create table STUDENT (
   ID_student           numeric(15)          identity,
   ID_grupa             numeric(10)          null,
   ID_tryb_nauki        numeric(10)          null,
   Imie_student         varchar(30)          null,
   Nazwisko_student     varchar(30)          null,
   Data_urodzenia       datetime             null,
   constraint PK_STUDENT primary key nonclustered (ID_student)
)
go

/*==============================================================*/
/* Index: "Student - Tryb_nauki_FK"                             */
/*==============================================================*/
create index "Student - Tryb_nauki_FK" on STUDENT (
ID_tryb_nauki ASC
)
go

/*==============================================================*/
/* Index: "Grupa - Student_FK"                                  */
/*==============================================================*/
create index "Grupa - Student_FK" on STUDENT (
ID_grupa ASC
)
go

/*==============================================================*/
/* Table: TRYB_NAUKI                                            */
/*==============================================================*/
create table TRYB_NAUKI (
   ID_tryb_nauki        numeric(10)          identity,
   Nazwa_tryb           varchar(20)          null,
   constraint PK_TRYB_NAUKI primary key nonclustered (ID_tryb_nauki)
)
go

alter table GRUPA
   add constraint "FK_GRUPA_SEMESTR -_SEMESTR" foreign key (ID_semestr)
      references SEMESTR (ID_semestr)
go

alter table "Grupa - Przedmiot"
   add constraint "FK_GRUPA - _GRUPA - P_GRUPA" foreign key (ID_grupa)
      references GRUPA (ID_grupa)
go

alter table "Grupa - Przedmiot"
   add constraint "FK_GRUPA - _GRUPA - P_PRZEDMIO" foreign key (ID_przedmiot)
      references PRZEDMIOT (ID_przedmiot)
go

alter table OCENA
   add constraint "FK_OCENA_OCENA - P_PRZEDMIO" foreign key (ID_przedmiot)
      references PRZEDMIOT (ID_przedmiot)
go

alter table OCENA
   add constraint "FK_OCENA_OCENA - S_STUDENT" foreign key (ID_student)
      references STUDENT (ID_student)
go

alter table PROWADZACY
   add constraint "FK_PROWADZA_PENSJA - _PENSJA" foreign key (ID_pensja)
      references PENSJA (ID_pensja)
go

alter table PRZEDMIOT
   add constraint FK_PRZEDMIO_PRZEDMIOT_PROWADZA foreign key (ID_prowadzacy)
      references PROWADZACY (ID_prowadzacy)
go

alter table PRZEDMIOT
   add constraint FK_PRZEDMIO_PRZEDMIOT_SEMESTR foreign key (ID_semestr)
      references SEMESTR (ID_semestr)
go

alter table "Prowadzacy - Student"
   add constraint FK_PROWADZA_PROWADZAC_PROWADZA foreign key (ID_prowadzacy)
      references PROWADZACY (ID_prowadzacy)
go

alter table "Prowadzacy - Student"
   add constraint FK_PROWADZA_PROWADZAC_STUDENT foreign key (ID_student)
      references STUDENT (ID_student)
go

alter table STUDENT
   add constraint "FK_STUDENT_GRUPA - S_GRUPA" foreign key (ID_grupa)
      references GRUPA (ID_grupa)
go

alter table STUDENT
   add constraint "FK_STUDENT_STUDENT -_TRYB_NAU" foreign key (ID_tryb_nauki)
      references TRYB_NAUKI (ID_tryb_nauki)
go

