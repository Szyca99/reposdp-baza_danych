import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws Exception {
        getConnection();
        insert();
        select();
        delete();
        select();

    }






// Połaczenie z Bazą danych SQL Server
    public static Connection getConnection() throws Exception {
        try {
            String url = "jdbc:sqlserver:// DESKTOP-DUNHU2G\\MSSQLSERVER;databaseName=Dziennik_Ocen";
            String username = "Marek1221";
            String password = "marek";
            Connection connection = DriverManager.getConnection(url, username, password);
            System.out.println("Połączono z SQL Server!");
            return  connection;

        } catch (Exception e){
            System.out.println("Coś poszło nie tak!");
            e.printStackTrace();
        }
            return null;
    }




// Dodanie kolejnego studenta do tabeli za pomocą "INSERT"
    public static void insert() throws Exception{
        final String imie = "Michał";
        final String nazwisko = "Nowacki";
        final String dataUrodzenia = "1987-03-30";

        try {
            Connection connection = getConnection();
            PreparedStatement inserted = connection.prepareStatement("INSERT INTO dbo.STUDENT (Imie_student, Nazwisko_student, Data_urodzenia) VALUES ('"+imie+"','"+nazwisko+"', '"+dataUrodzenia+"')");
            inserted.executeUpdate();

        }catch (Exception exception1){
            System.out.println("Nie udało się zrobić INSERT!");
            exception1.printStackTrace();
        }

        finally {
            System.out.println("INSERT zrobiony!");
        }

    }




    //      Wyświetlenie tabeli z bazy danych SQL Servera
    public static ArrayList<String> select() throws Exception{
        try{
            Connection connection = getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT  Imie_student, Nazwisko_student, Data_urodzenia FROM dbo.STUDENT");
            ResultSet result = statement.executeQuery();

            ArrayList<String> array = new ArrayList<String>();

            while (result.next()){
                System.out.println(result.getString("Imie_student"));
                System.out.println(result.getString("Nazwisko_student"));
                System.out.println(result.getString("Data_urodzenia"));
                System.out.println(" ");

                array.add(result.getString("Imie_student"));
                array.add(result.getString("Nazwisko_student"));
                array.add(result.getString("Data_urodzenia"));
            }

            System.out.println("Wyświetlono wszystkie elementy tabeli!");

            return array;

        }catch (Exception exception){
            exception.printStackTrace();
            System.out.println("Nie udało się zrobić SELECT-a");
        }

        return null;
    }




    // Usuniecie z tabeli elementów o określonych indeksach (ID>4)
    public static void delete() throws Exception{
        final String imie = "Michał";
        final String nazwisko = "Nowacki";
        final String dataUrodzenia = "1987-03-30";

        try {
            Connection connection = getConnection();
            PreparedStatement inserted = connection.prepareStatement("DELETE FROM dbo.STUDENT WHERE ID_student >4");
            inserted.executeUpdate();

        }catch (Exception exception1){
            System.out.println("Nie udało się zrobić DELETE!");
            exception1.printStackTrace();
        }

        finally {
            System.out.println("DELETE zrobiony!");
        }

    }

}
